# Web Socket server

This is a socket server that receives messages and dispatches them to a queue for being processed.


## Compile


```
. ./compile.sh
```



## Run


```
bin/socket-service
```
