package main

import (
	"log"
	"fmt"
	"net/http"
	"github.com/googollee/go-socket.io"
	"github.com/streadway/amqp"
	"io/ioutil"
	"os"
	"encoding/xml"
	"encoding/json"
)

type Configuration struct {
	RABBIT_USER string `xml:"rabbit_user"`
	RABBIT_PWD string `xml:"rabbit_pwd"`
	RABBIT_HOST string `xml:"rabbit_host"`
	RABBIT_PORT string `xml:"rabbit_port"`
	RABBIT_QUEUE_NAME string `xml:"rabbit_queue_name"`
	PORT string `xml:"port"`
	DISCONNECT_EVENT_TYPE string `xml:"disconnect_event_type"`
	CONNECT_EVENT_TYPE string `xml:"connect_event_type"`
}

var config Configuration

func getConfiguration() {
    // get credentials from file
    configDataXML, err := ioutil.ReadFile("./config.xml")

    if err != nil {
        fmt.Println("Config not found.")
        os.Exit(1)
	}
	
	config.RABBIT_HOST = os.Getenv("RABBIT_HOST")
	config.RABBIT_PORT = os.Getenv("RABBIT_PORT")
	config.RABBIT_USER = os.Getenv("RABBIT_USER")
	config.RABBIT_PWD = os.Getenv("RABBIT_PWD")
	config.RABBIT_QUEUE_NAME = os.Getenv("RABBIT_QUEUE_NAME")
	config.PORT = os.Getenv("PORT")
	config.DISCONNECT_EVENT_TYPE = os.Getenv("DISCONNECT_EVENT_TYPE");
	config.CONNECT_EVENT_TYPE = os.Getenv("CONNECT_EVENT_TYPE");
	
	
	// xml.Unmarshal(configDataXML, &config)
}

func main() {
	getConfiguration()
	
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}
	server.On("connection", func(so socketio.Socket) {
		so.On("register", func(msg string) {
			log.Println("NEW REGISTER")
			var msgMap map[string]interface{}

			var contentMap map[string]string
			contentMap = make(map[string]string)
			contentMap["TYPE"] = config.CONNECT_EVENT_TYPE

			err := json.Unmarshal([]byte(msg), &msgMap)

			if err != nil {
				log.Println(err)
			}

			msgMap["socketId"] = so.Id()
			msgMap["content"] = contentMap

			jsonMsg, _ := json.Marshal(msgMap)

			publishMessageToRabbit(string(jsonMsg))
		})
		so.On("message", func(msg string) {
			log.Println("NEW MESSAGE")
			publishMessageToRabbit(msg)
		})
		so.On("disconnection", func() {

			var msgMap map[string]interface{}
			msgMap = make(map[string]interface{})

			var contentMap map[string]string
			contentMap = make(map[string]string)

			log.Println("on disconnect")


			msgMap["socketId"] = so.Id()

			contentMap["TYPE"] = config.DISCONNECT_EVENT_TYPE
			msgMap["content"] = contentMap

			jsonMsg, _ := json.Marshal(msgMap)

			publishMessageToRabbit(string(jsonMsg))
		})
	})
	server.On("error", func(so socketio.Socket, err error) {
		log.Println("error:", err)
	})

	http.Handle("/", server)
	log.Fatal(http.ListenAndServe(":"+ config.PORT, nil))
}

func publishMessageToRabbit(msg string) {

	var rabbitConnectionString = "amqp://"+config.RABBIT_USER+":"+ config.RABBIT_PWD + "@"+config.RABBIT_HOST+":"+config.RABBIT_PORT+"/"
	
	conn, err := amqp.Dial(rabbitConnectionString)

	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()
	ch, err := conn.Channel()
	failOnError(err, "Failed to open to channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		config.RABBIT_QUEUE_NAME, // name
		true,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	body := msg
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing {
		ContentType: "application/json",
		Body:        []byte(body),
		})

	failOnError(err, "Failed to publish a message")
}

func failOnError(err error, msg string) {
  if err != nil {
    log.Fatalf("%s: %s", msg, err)
    panic(fmt.Sprintf("%s: %s", msg, err))
  }
}
