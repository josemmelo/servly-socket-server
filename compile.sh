#!/bin/sh

mkdir bin
cd bin

# get dependencies
go get github.com/googollee/go-socket.io
go get github.com/streadway/amqp

# compile
go build ../socket-service.go

cd ..
