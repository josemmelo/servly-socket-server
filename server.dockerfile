FROM golang:latest 

RUN mkdir /home/app/
ADD ./ /home/app/ 
WORKDIR /home/app/

RUN chmod +x ./compile.sh
RUN ./compile.sh

RUN chmod +x bin/socket-service

RUN chmod +x ./start.sh
CMD ./start.sh